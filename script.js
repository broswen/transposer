const notemap = {"C":0, "C#":1, "D":2, "D#":3, "E":4, "F":5, "F#":6, "G":7, "G#":8, "A":9, "A#":10, "B":11};
const numbermap = {0:"C", 1:"C#", 2:"D", 3:"D#", 4:"E", 5:"F", 6:"F#", 7:"G", 8:"G#", 9:"A", 10:"A#", 11:"B"};
var btn, output, input, interval;

function validate(input){
  var list = input.split(" ");
  for(item of list){
    if(notemap[item.toUpperCase()] == undefined) return false;
  }
  return true;
}

function transposeNotes(notes, interval){
  var transposed = "";
  interval = interval < 0 ? 12+parseInt(interval) : interval;

  for(note of notes.split(" ")){
    transposed += " " + numbermap[parseInt(notemap[note.toUpperCase()] + parseInt(interval)) % 12];
  }
  return transposed;
}

function updateTranspose(){
  if(validate(input.value.trim()) && !isNaN(parseInt(interval.value))){
    output.innerHTML = transposeNotes(input.value.trim(), interval.value);
    input.value = input.value.toUpperCase();
  }else{
    output.innerHTML = "Error parsing notes (or interval)...";
  }
}
window.onload = function() {

  btn = document.getElementById('notes-button');
  interval = document.getElementById("notes-interval");
  output = document.getElementById('notes-output');
  input = document.getElementById('notes-input');

  input.addEventListener("input", function(){updateTranspose()});
  interval.addEventListener("input", function(){updateTranspose()});
  btn.addEventListener("click", function(){updateTranspose()});
};
